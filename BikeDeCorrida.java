
//É uma...
public class BikeDeCorrida extends Bike {

    private final int CAPACIDADE_DE_CARGA = 60;

    public BikeDeCorrida(String cor, int velocidadeMaxima) {
        // Chamada do construtor da classe mãe.
        super(cor, velocidadeMaxima);
    }

    // Implementação da interface transporte
    public int getCapacidadeDeCarga() {
        return CAPACIDADE_DE_CARGA;
    }

    @Override
    public String toString() {

        return "BikeDeCorrida [cor=" + super.getCor()
                + ", velocidadeAtual=" + velocidadeAtual
                + ", velocidadeMaxima=" + velocidadeMaxima + "]";
    }

}
