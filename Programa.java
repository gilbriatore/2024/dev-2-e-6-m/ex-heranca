
//Imports necessários para trabalhar com listas dinâmicas
import java.util.List;
import java.util.ArrayList;

public class Programa {

    public static void main(String[] args) throws Exception {

        System.out.println();

        // Bike bikePrincipal = new Bike("Preta", 30);

        // Listas dinâmicas
        List<Bike> listaDeBikesGenerica = new ArrayList<>();
        // List<BikeDeCorrida> listaBikesDeCorrida = new ArrayList<>();

        BikeDeCorrida bikeDeCorrida = new BikeDeCorrida("Azul", 100);
        // bikeDeCorrida.setCor("Azul");
        bikeDeCorrida.setVelocidadeAtual(80);

        listaDeBikesGenerica.add(bikeDeCorrida);

        BikeDePasseio bikeDePasseio = new BikeDePasseio("Verde", 40);
        // bikeDePasseio.setCor("Verde");
        // bikeDePasseio.setVelocidade(40);

        listaDeBikesGenerica.add(bikeDePasseio);

        BikeDeTrilha bikeDeTrilha = new BikeDeTrilha("Amarela", 60);
        // bikeDeTrilha.setCor("Amarela");
        // bikeDeTrilha.setVelocidade(60);

        listaDeBikesGenerica.add(bikeDeTrilha);

        // System.out.println(bikePrincipal);
        // System.out.println(bikeDeCorrida);
        // System.out.println(bikeDePasseio);
        // System.out.println(bikeDeTrilha);

        for (Bike bike : listaDeBikesGenerica) {
            System.out.println(bike);
        }

    }

}
