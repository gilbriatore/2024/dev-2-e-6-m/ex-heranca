
//É uma...
public class BikeDePasseio extends Bike {

    private final int CAPACIDADE_DE_CARGA = 50;

    public BikeDePasseio(String cor, int velocidadeMaxima) {
        // Chamada do construtor da classe mãe.
        super(cor, velocidadeMaxima);
    }

    //Implementação da interface transporte
    public int getCapacidadeDeCarga(){
        return CAPACIDADE_DE_CARGA;
    }

    @Override
    public String toString() {

        return "BikeDePasseio [cor=" + super.getCor()
                + ", velocidadeAtual=" + velocidadeAtual
                + ", velocidadeMaxima=" + velocidadeMaxima + "]";
    }

}
