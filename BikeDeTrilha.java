
//É uma...
public class BikeDeTrilha extends Bike {

    private final int CAPACIDADE_DE_CARGA = 80;

    public BikeDeTrilha(String cor, int velocidadeMaxima) {
        // Chamada do construtor da classe mãe.
        super(cor, velocidadeMaxima);
    }

    // Implementação da interface transporte
    public int getCapacidadeDeCarga() {
        return CAPACIDADE_DE_CARGA;
    }

    @Override
    public String toString() {

        return "BikeDeTrilha [cor=" + super.getCor()
                + ", velocidadeAtual=" + velocidadeAtual
                + ", velocidadeMaxima=" + velocidadeMaxima + "]";
    }

}
