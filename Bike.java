
public abstract class Bike implements Transporte {

    private String cor;
    protected int velocidadeAtual;
    protected int velocidadeMaxima;

    // public Bike() {
    // }

    // public Bike(String cor) {
    // this.cor = cor;
    // }

    public Bike(String cor, int velocidadeMaxima) {
        this.cor = cor;
        this.velocidadeMaxima = velocidadeMaxima;
    }

    public String getCor() {
        return cor;
    }

    public void setCor(String cor) {
        this.cor = cor;
    }

    public int getVelocidadeAtual() {
        return velocidadeAtual;
    }

    public void setVelocidadeAtual(int velocidadeAtual) {
        this.velocidadeAtual = velocidadeAtual;
    }

    @Override
    public String toString() {
        return "Bike [cor=" + cor + ", velocidadeAtual=" + velocidadeAtual + ", velocidadeMaxima=" + velocidadeMaxima
                + "]";
    }

}
